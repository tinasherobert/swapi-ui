import { styled } from "goober";

export const ModalContainer = styled("section")`
padding: 16px;
    width: 52%;
    z-index: 10;
    background: #000;
    position: absolute;
    /* display: none; */
    position: fixed;
    height: 90%;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.9);
`;

export const ModalTitle = styled("section")`
  display: grid;
  grid-template-columns: auto 1fr auto;
  grid-template-rows: auto;
  grid-column-gap: 0px;
  grid-row-gap: 0px;
  width: 100%;
  color: #ffd523;
`;

export const ModalLabel = styled("h1")`
  color: #ffd523;
  font-size: 20px;
`;

export const ModalClose = styled("button")`
  outline: none;
  border: none;
  font-size: 20px;
  color: #fff;
  height: auto;
`;

export const ModalContent = styled("section")`
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 80%; /* Could be more or less, depending on screen size */
`;
